package main

import (
	"fmt"
	"log"
	"reflect"
	"strings"

	"github.com/spf13/cobra"
)

/*
constants used as the flag names
*/
const (
	configSomeBool        = "some-bool"
	configSomeBoolSlice   = "some-bool-slice"
	configSomeFloat       = "some-float"
	configSomeFloatSlice  = "some-float-slice"
	configSomeInt         = "some-int"
	configSomeIntSlice    = "some-int-slice"
	configSomeString      = "some-string"
	configSomeStringSlice = "some-string-slice"
)

// configurationProperties defines the structure of configurations
type configurationProperties struct {
	SomeBool        bool
	SomeBoolSlice   []bool
	SomeFloat       float64
	SomeFloatSlice  []float64
	SomeInt         int64
	SomeIntSlice    []int64
	SomeString      string
	SomeStringSlice []string
}

var Command *cobra.Command
var configuration configurationProperties

func init() {
	Command = &cobra.Command{
		Use: "cobra",
		RunE: func(cmd *cobra.Command, args []string) error {
			// this part is complex but is just for printing of the values,
			// remove!!!
			configurationType := reflect.TypeOf(configuration)
			configurationValue := reflect.ValueOf(configuration)
			for i := 0; i < configurationType.NumField(); i++ {
				var output strings.Builder
				field := configurationType.FieldByIndex([]int{i})
				value := configurationValue.FieldByIndex([]int{i})
				kind := field.Type.Kind()
				switch kind {
				case reflect.Slice:
					kind = field.Type.Elem().Kind()
					switch kind {
					case reflect.Bool:
						val := value.Interface().([]bool)
						output.WriteString(fmt.Sprintf("%s.%s: %s%v", configurationType.Name(), field.Name, kind, val))
					case reflect.Float32, reflect.Float64:
						val := value.Interface().([]float64)
						output.WriteString(fmt.Sprintf("%s.%s: %s%v", configurationType.Name(), field.Name, kind, val))
					case reflect.Int, reflect.Int16, reflect.Int32, reflect.Int64:
						val := value.Interface().([]int64)
						output.WriteString(fmt.Sprintf("%s.%s: %s%v", configurationType.Name(), field.Name, kind, val))
					case reflect.String:
						val := value.Interface().([]string)
						output.WriteString(fmt.Sprintf("%s.%s: %s%v", configurationType.Name(), field.Name, kind, val))
					}
				case reflect.Bool:
					output.WriteString(fmt.Sprintf("%s.%s: %v", configurationType.Name(), field.Name, value.Bool()))
				case reflect.Float32, reflect.Float64:
					output.WriteString(fmt.Sprintf("%s.%s: %v", configurationType.Name(), field.Name, value.Float()))
				case reflect.Int, reflect.Int16, reflect.Int32, reflect.Int64:
					output.WriteString(fmt.Sprintf("%s.%s: %v", configurationType.Name(), field.Name, value.Int()))
				case reflect.String:
					output.WriteString(fmt.Sprintf("%s.%s: %s", configurationType.Name(), field.Name, value.String()))
				default:
					output.WriteString(fmt.Sprintf("%s.%s: unknown", configurationType.Name(), field.Name))
				}
				log.Println(output.String())
			}
			return cmd.Help()
		},
	}

	// add the flags to the command
	Command.Flags().BoolVarP(&configuration.SomeBool, configSomeBool, "b", false, "some bool")
	Command.Flags().BoolSliceVarP(&configuration.SomeBoolSlice, configSomeBoolSlice, "B", []bool{false, true, false}, "some bool slice")
	Command.Flags().Float64VarP(&configuration.SomeFloat, configSomeFloat, "f", 3.142, "some float")
	Command.Flags().Float64SliceVarP(&configuration.SomeFloatSlice, configSomeFloatSlice, "F", []float64{3.142, 1.512, 690.42}, "some float slice")
	Command.Flags().Int64VarP(&configuration.SomeInt, configSomeInt, "i", 42, "some int")
	Command.Flags().Int64SliceVarP(&configuration.SomeIntSlice, configSomeIntSlice, "I", []int64{42, 43, 44}, "some int slice")
	Command.Flags().StringVarP(&configuration.SomeString, configSomeString, "s", "hello world", "some string")
	Command.Flags().StringSliceVarP(&configuration.SomeStringSlice, configSomeStringSlice, "S", []string{"hello", "mad", "world"}, "some string slice")
}

func main() {
	if err := Command.Execute(); err != nil {
		log.Fatalf("failed to complete successfully: %s", err)
	}
}
