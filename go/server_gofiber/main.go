package main

import (
	"fmt"
	"log"
	"os"
	"os/signal"
	"strconv"
	"sync"
	"syscall"
	"time"

	"github.com/gofiber/adaptor/v2"
	"github.com/gofiber/fiber/v2"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/spf13/viper"
)

func main() {
	// configuration
	viper.AutomaticEnv()
	viper.SetEnvPrefix("server")
	viper.SetDefault("port", 8888)
	port := viper.GetInt("port")
	viper.SetDefault("address", "0.0.0.0")
	addr := viper.GetString("address")

	// observability
	httpRequests := promauto.NewCounterVec(prometheus.CounterOpts{
		Name: "http_requests",
		Help: "Number of HTTP requests handled",
	}, []string{"path", "method", "status"})

	// application init
	app := fiber.New(fiber.Config{
		// application
		AppName: "default",

		// server
		DisableStartupMessage:   true,
		BodyLimit:               5 * 1024 * 1024, // 5mb
		ReadBufferSize:          8192,
		WriteBufferSize:         8192,
		EnableTrustedProxyCheck: true,
		TrustedProxies:          []string{},

		// timeouts
		IdleTimeout:  15 * time.Second,
		ReadTimeout:  15 * time.Second,
		WriteTimeout: 15 * time.Second,
	})

	// lifecycle
	osSignals := make(chan os.Signal, 1)
	signal.Notify(osSignals, syscall.SIGINT, syscall.SIGTERM)
	var waiter sync.WaitGroup
	go func() {
		waiter.Add(1)
		defer waiter.Done()
		<-osSignals
		log.Printf("waiting for server graceful shutdown...")
		if err := app.Shutdown(); err != nil {
			log.Fatalf("failed to shutdown gracefully: %s", err)
		}
		log.Printf("server gracefully shut down")
	}()

	// application middlewares
	app.Use(func(c *fiber.Ctx) error {
		defer func() {
			httpRequests.With(prometheus.Labels{
				"path":   string(c.Request().URI().Path()),
				"method": string(c.Request().Header.Method()),
				"status": strconv.Itoa(c.Response().StatusCode()),
			}).Inc()
		}()
		return c.Next()
	})

	// application routing
	app.Get("/", func(c *fiber.Ctx) error {
		return c.Send([]byte("hello world"))
	})
	app.Post("/api", func(c *fiber.Ctx) error {
		body := string(c.Body())
		contentLength := c.Request().Header.ContentLength()
		log.Printf("received body of size %v: %s", contentLength, body)
		return nil
	})
	app.Get("/api/:pathParam", func(c *fiber.Ctx) error {
		log.Printf("- - - behold, headers of %s - - -", string(c.Request().Header.Peek("host")))
		c.Request().Header.VisitAll(func(key, value []byte) {
			log.Printf("%s: %s", key, value)
		})

		pathParam := c.Params("pathParam")
		return c.Send([]byte(fmt.Sprintf("hello %s", pathParam)))
	})
	app.Get("/metrics", adaptor.HTTPHandler(promhttp.Handler()))
	app.Get("/healthz", func(c *fiber.Ctx) error { return nil })
	app.Get("/readyz", func(c *fiber.Ctx) error {
		return nil
	})

	bindAddr := fmt.Sprintf("%s:%v", addr, port)
	log.Printf("starting server on %s", bindAddr)
	if err := app.Listen(bindAddr); err != nil {
		log.Fatalf("application stopped: %s", err)
	}
	waiter.Wait()
	log.Printf("bye")
}
