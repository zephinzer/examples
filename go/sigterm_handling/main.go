package main

import (
	"fmt"
	"log"
	"os"
	"os/signal"
	"syscall"
	"time"
)

func main() {
	log.Printf("use ctrl+c to exit\n")
	sig := make(chan os.Signal, 1)
	signal.Notify(sig, syscall.SIGINT, syscall.SIGTERM)

	go func() {
		received := <-sig
		fmt.Printf("received signal %v\n", received.String())
		os.Exit(1)
	}()

	for {
		<-time.After(1 * time.Hour)
	}
}
