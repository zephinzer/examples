package main

import (
	"context"
	"log"

	"github.com/aws/aws-sdk-go-v2/config"
	"github.com/aws/aws-sdk-go-v2/service/sts"
)

func main() {
	conf, err := config.LoadDefaultConfig(context.Background())
	if err != nil {
		panic(err)
	}
	stsClient := sts.NewFromConfig(conf)
	callerIdentity, err := stsClient.GetCallerIdentity(context.Background(), &sts.GetCallerIdentityInput{})
	if err != nil {
		panic(err)
	}
	log.Printf("current account: %v", *callerIdentity.Account)
	log.Printf("current arn: %v", *callerIdentity.Arn)
	log.Printf("current user id: %v", *callerIdentity.UserId)
}
