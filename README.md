# Examples... of Everything

This is repository contains examples of everything and hopefully serves as a quick start for almost any technical adventure you may have.

# Structure & Organisation

This repository is structured using `/technology/path` with the final sub-directory being the name of the example.

For example:

- `/go/server_gofiber` signals a server example written in Go 
- `/docker-compose/nats` signals a `docker-compose.yml` that brings up NATS
- `/temporal/go/basic` signals a basic example written in Go for Temporal

Also, since these are reference implementations meant for copy-pasting before extension/restructuring, as far as possible, all examples should be kept to a single file to improve readability. No best-practices exist except for "does this enable a viewer to more easily see how everything is linked?"

# Contributing

Obviously I can't use and learn every technology in here so contributions to technologies not here are more than welcome.
