package main

import (
	"context"
	"fmt"
	"log"
	"time"

	"github.com/google/uuid"
	"go.temporal.io/sdk/client"
	"go.temporal.io/sdk/worker"
	"go.temporal.io/sdk/workflow"
)

func Basic() (string, error) {
	return "basic af you", nil
}

func BasicWorkflow(ctx workflow.Context) (string, error) {
	options := workflow.ActivityOptions{
		StartToCloseTimeout: 5 * time.Second,
	}
	ctx = workflow.WithActivityOptions(ctx, options)
	var result string
	err := workflow.ExecuteActivity(ctx, Basic).Get(ctx, &result)
	return result, err
}

func main() {
	c, err := client.Dial(client.Options{})
	if err != nil {
		panic(err)
	}
	defer c.Close()

	queueName := "basic_task_queue"

	// sample function that runs every 5 seconds
	go func() {
		runId := ""
		firstRun := true
		for {
			if !firstRun {
				<-time.After(5 * time.Second)
			}
			firstRun = false
			runId = uuid.NewString()
			response, err := c.ExecuteWorkflow(
				context.Background(),
				client.StartWorkflowOptions{
					ID:        fmt.Sprintf("basic-%s", runId),
					TaskQueue: queueName,
				},
				BasicWorkflow,
			)
			if err != nil {
				log.Printf("failed to complete workflow: %s", err)
				continue
			}
			var result string
			if err := response.Get(
				context.Background(),
				&result,
			); err != nil {
				log.Printf("failed to get result: %s", err)
				continue
			}
			log.Printf("result: %s", result)
		}
	}()

	w := worker.New(c, queueName, worker.Options{})
	w.RegisterWorkflow(BasicWorkflow)
	w.RegisterActivity(Basic)

	if err = w.Run(worker.InterruptCh()); err != nil {
		panic(err)
	}
}
